#!/bin/bash

VERSION=$1

# Get the current version if version number is not specified
if [ -z $VERSION ]; then
    VERSION=`sed '5q;d' pom.xml | sed 's/.*<version>\(.*\)<\/version>/\1/'`
fi

curl -u admin:admin -F file=@"$HOME/.m2/repository/com/northps/cq/url-demo-package/$VERSION/url-demo-package-$VERSION.zip" -F name="url-demo" -F force=true -F install=true http://localhost:4502/crx/packmgr/service.jsp
