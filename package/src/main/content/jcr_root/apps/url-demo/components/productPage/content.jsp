<%@include file="/libs/foundation/global.jsp" %>
<%
    String name = properties.get("name", "Procuct name not available.");
    String description = properties.get("description", "Description not available.");
	String price = properties.get("price", "Price not available.");
%>
<h1>Product Page</h1>
<h2><%= name %></h2>
<p><%= description %></p>
<p>Price: $<%= price %></p>