package com.northps.cq.urldemo.generatedproduct;

import org.apache.sling.adapter.annotations.Adaptable;
import org.apache.sling.adapter.annotations.Adapter;
import org.apache.sling.api.resource.AbstractResource;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

import com.day.cq.wcm.api.Page;

@Adaptable(adaptableClass=Resource.class, adapters={
    @Adapter({ValueMap.class}),
    @Adapter({Page.class})
})
public class ProductResource extends AbstractResource implements Resource {
	
    private final String path;
    private final ResourceMetadata metadata;
    private final ValueMap valueMap;
    private final ResourceResolver resolver;
    
    // How to render this resource
    public static final String RESOURCE_TYPE = "url-demo/components/productPage";
    
    ProductResource(ResourceResolver resolver, String path, ValueMap valueMap) {
        this.path = path;
                
        this.valueMap = valueMap;
        this.resolver = resolver;
        
        metadata = new ResourceMetadata();
        metadata.setResolutionPath(path);
    }
    
    @Override
    public String toString() {
        return getClass().getSimpleName() + " " + path;
    }
    
    public String getPath() {
        return path;
    }

    public ResourceMetadata getResourceMetadata() {
        return metadata;
    }

    public ResourceResolver getResourceResolver() {
        return resolver;
    }

    public String getResourceSuperType() {
        return null;
    }

    public String getResourceType() {
        return RESOURCE_TYPE;
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public <AdapterType> AdapterType adaptTo(Class<AdapterType> type) {
        if (type == ValueMap.class) {
            return (AdapterType)valueMap;
        } else if (type == Page.class) {
        	return (AdapterType)(new ProductPage(this, valueMap));
        }
        return super.adaptTo(type);
    }
}