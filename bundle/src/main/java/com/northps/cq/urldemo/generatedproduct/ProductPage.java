package com.northps.cq.urldemo.generatedproduct;

import java.util.Calendar;
import java.util.Iterator;
import java.util.Locale;

import org.apache.sling.adapter.annotations.Adaptable;
import org.apache.sling.adapter.annotations.Adapter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.day.cq.commons.Filter;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.Template;
import com.day.cq.wcm.api.WCMException;

@Adaptable(adaptableClass=ProductPage.class, adapters={
    @Adapter({Resource.class})
})
class ProductPage implements Page {
	private ProductResource resource;
	private ValueMap valueMap;
	
	public ProductPage(ProductResource resource, ValueMap valueMap) {
		this.resource = resource;
		this.valueMap = valueMap; 
	}
	
    @SuppressWarnings("unchecked")
	public <AdapterType> AdapterType adaptTo(Class<AdapterType> type) {
        if (type == Resource.class) {
        	return (AdapterType)resource;
        }
        return null;
	}

	public boolean canUnlock() {
		// Cannot unlock. Fixed content.
		return false;
	}

	/**
	 * @see http://dev.day.com/docs/en/cq/current/javadoc/com/day/cq/wcm/api/Page.html
	 */
	public Page getAbsoluteParent(int level) {
		String thisRoot = ProductResourceProvider.ROOT;
		if (thisRoot.indexOf('/') == 0) thisRoot = thisRoot.substring(1);
		String pathElements[] = thisRoot.split("/");
		
		if (level < pathElements.length) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i <= level; i++)
				sb.append('/').append(pathElements[i]);
			String path = sb.toString();
			return resource.getResourceResolver().resolve(path).adaptTo(Page.class);
		} else {
			return null;
		}
	}

	public Resource getContentResource() {
		return new ProductPageContent(resource, valueMap);
	}

	public Resource getContentResource(String path) {
		return getContentResource().getChild(path);
	}

	public int getDepth() {
		// (jcr_root)/content/url-demo/generated-products/(blabla)
		// 1         /2      /3       /4                 /5
		return 5;
	}

	public Locale getLanguage(boolean arg0) {
		return new Locale("en", "US");
	}

	public Calendar getLastModified() {
		return (Calendar)valueMap.get("cq:lastModified");
	}

	public String getLastModifiedBy() {
		return (String)valueMap.get("cq:lastModifiedBy");
	}

	public String getLockOwner() {
		return null;
	}

	public String getName() {
		return resource.getPath().substring(ProductResourceProvider.ROOT.length());
	}

	public String getNavigationTitle() {
		return null;
	}

	public Calendar getOffTime() {
		return null;
	}

	public Calendar getOnTime() {
		return null;
	}

	public PageManager getPageManager() {
		return resource.getResourceResolver().adaptTo(PageManager.class);
	}

	public String getPageTitle() {
		return (String)valueMap.get("name");
	}

	public Page getParent() {
		return getParent(1);
	}

	public Page getParent(int level) {
		// jcr_root + self = 2
		return getAbsoluteParent(getDepth() - 2 - level);
	}

	public String getPath() {
		return resource.getPath();
	}

	public ValueMap getProperties() {
		return valueMap;
	}

	public ValueMap getProperties(String path) {
		return getProperties();
	}

	public Tag[] getTags() {
		return new Tag[] {};
	}

	public Template getTemplate() {
		return null;
	}

	public String getTitle() {
		return getPageTitle();
	}

	public String getVanityUrl() {
		return null;
	}

	public boolean hasChild(String path) {
		if (path.equals("jcr:content")) {
			return true;
		}
		return false;
	}

	public boolean hasContent() {
		return true;
	}

	public boolean isHideInNav() {
		return false;
	}

	public boolean isLocked() {
		return false;
	}

	public boolean isValid() {
		return true;
	}

	public Iterator<Page> listChildren() {
		// No children
		return null;
	}

	public Iterator<Page> listChildren(Filter<Page> arg0) {
		return null; 
	}

	public Iterator<Page> listChildren(Filter<Page> arg0, boolean arg1) {
		return null; 
	}

	public void lock() throws WCMException {
		// Do nothing
	}

	public long timeUntilValid() {
		// It is valid
		return 0;
	}

	public void unlock() throws WCMException {
		// Do nothing
	}

	public String getDescription() {
		return (String)valueMap.get("description");
	}
}