package com.northps.cq.urldemo.shared;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.rewriter.Transformer;
import org.apache.sling.rewriter.TransformerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(metatype = true, immediate = true, label = "URL-Demo Link Transformer Factory", description = "Ensures product URLs are properly applied")
@Service(value = TransformerFactory.class)
@Properties({
		@Property(name = "service.pid", value = "com.northps.cq.urldemo.ProductTransformerFactory", propertyPrivate = false),
		@Property(name = "service.description", value = "URL-Demo product url rewriter", propertyPrivate = false),
		@Property(name = "service.vendor", value = "NorthPoint", propertyPrivate = false),
		@Property(name = "pipeline.type", value = "url-demo-transformer", propertyPrivate = false),
		@Property(name = "service.ranking", value = "-1000", propertyPrivate = false) })
public class ProductTransformerFactory implements TransformerFactory {
	private static final Logger log = LoggerFactory
			.getLogger(ProductTransformerFactory.class);

	public Transformer createTransformer() {
		log.debug("Return ProductTransformer.");
		return new ProductTransformer();
	}
}
