package com.northps.cq.urldemo.utils;

public interface CacheInvalidator {
	public void invalidate(String[] flushes, String... toInvalidatePaths);
	public void invalidateWithVanities(String[] flushes, String... toInvalidatePaths);
}
