package com.northps.cq.urldemo.activator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
	private static final Logger log = LoggerFactory.getLogger(Activator.class);

	public void start(BundleContext context) throws Exception {
		log.info(context.getBundle().getSymbolicName() + " started.");
	}

	public void stop(BundleContext context) throws Exception {
		log.info(context.getBundle().getSymbolicName() + " stopped.");
	}

}
