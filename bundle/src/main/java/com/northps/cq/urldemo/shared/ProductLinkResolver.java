package com.northps.cq.urldemo.shared;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.northps.cq.urldemo.utils.HashGenerator;

public class ProductLinkResolver {
	private static final String FILTER_PRODUCT_INTERNAL_PREFIX = "/content/url-demo/filter-products-repo/";
	private static final String FILTER_PRODUCT_EXTERNAL_PREFIX = "/content/url-demo/filter-products/";
	private static final String JCR_PRODUCT_INTERNAL_PREFIX = "/content/url-demo/jcr-products-repo/";
	private static final String JCR_PRODUCT_EXTERNAL_PREFIX = "/content/url-demo/jcr-products/";
	private static final String MAP_PRODUCT_INTERNAL_PREFIX = "/content/url-demo/map-products-repo/";
	private static final String MAP_PRODUCT_EXTERNAL_PREFIX = "/content/url-demo/map-products/";

	private static Logger log = LoggerFactory.getLogger(ProductLinkResolver.class);

	// External URL to internal path. Example:
	// /content/url-demo/filter-products/circle
	// ==>
	// /content/url-demo/filter-products-repo/72/circle
	private static String hashResolve(String externalPath,
			String internalPrefix, String externalPrefix) {
		log.debug("externalPath = " + externalPath);

		int dotPos = externalPath.indexOf('.');
		String nodeName = dotPos == -1 ? externalPath.substring(externalPrefix
				.length()) : externalPath.substring(externalPrefix.length(),
				dotPos);
		String suffix = dotPos == -1 ? "" : externalPath.substring(dotPos);

		String lastTwoHash = HashGenerator.hashLast(nodeName, 2);
		StringBuilder sb = new StringBuilder(internalPrefix);
		sb.append(lastTwoHash).append('/').append(nodeName).append(suffix);
		String internalPath = sb.toString();

		log.debug("internalPath = " + internalPath);
		return internalPath;
	}

	// Returns null if nothing special happens
	// for faster processing of the caller
	public static String resolve(String externalPath) {
		String internalPath = null;
		// Filter products
		if (externalPath
				.indexOf(ProductLinkResolver.FILTER_PRODUCT_EXTERNAL_PREFIX) != -1) {
			internalPath = hashResolve(externalPath,
					ProductLinkResolver.FILTER_PRODUCT_INTERNAL_PREFIX,
					ProductLinkResolver.FILTER_PRODUCT_EXTERNAL_PREFIX);
		} else if (externalPath
				.indexOf(ProductLinkResolver.JCR_PRODUCT_EXTERNAL_PREFIX) != -1) {
			internalPath = hashResolve(externalPath,
					ProductLinkResolver.JCR_PRODUCT_INTERNAL_PREFIX,
					ProductLinkResolver.JCR_PRODUCT_EXTERNAL_PREFIX);
		}
		// Map products are handled by /etc/map.
		// Nothing to do here.

		log.debug("internalPath = " + internalPath);
		return internalPath;
	}
	// Internal path to external URL. Example:
	// /content/url-demo/filter-products-repo/72/circle
	// ==>
	// /content/url-demo/filter-products/circle
	private static String hashMap(String internalPath, String internalPrefix,
			String externalPrefix) {
		final int beginPos = internalPath.indexOf(internalPrefix);
		final int endPos = beginPos + internalPrefix.length() + 3;
		StringBuilder sb = new StringBuilder(
				internalPath.substring(0, beginPos));
		sb.append(externalPrefix);
		sb.append(internalPath.substring(endPos));
		String externalPath = sb.toString();

		return externalPath;
	}

	// Returns null if nothing special happens
	// for faster processing of the caller
	public static String map(String internalPath) {
		log.debug("internalPath = " + internalPath);
		String externalPath = null;
		if (internalPath
				.indexOf(ProductLinkResolver.FILTER_PRODUCT_INTERNAL_PREFIX) != -1) {
			// Filter Products
			externalPath = hashMap(internalPath,
					ProductLinkResolver.FILTER_PRODUCT_INTERNAL_PREFIX,
					ProductLinkResolver.FILTER_PRODUCT_EXTERNAL_PREFIX);
			log.debug("Filter products: externalPath = " + externalPath);
		} else if (internalPath
				.indexOf(ProductLinkResolver.JCR_PRODUCT_INTERNAL_PREFIX) != -1) {
			// JCR Products
			externalPath = hashMap(internalPath,
					ProductLinkResolver.JCR_PRODUCT_INTERNAL_PREFIX,
					ProductLinkResolver.JCR_PRODUCT_EXTERNAL_PREFIX);
			log.debug("JCR products: externalPath = " + externalPath);
		} else if (internalPath
				.indexOf(ProductLinkResolver.MAP_PRODUCT_INTERNAL_PREFIX) != -1) {
			// Map products
			log.debug("internalPath = " + internalPath);

			final int beginPos = internalPath
					.indexOf(ProductLinkResolver.MAP_PRODUCT_INTERNAL_PREFIX);
			final int endPos = beginPos
					+ ProductLinkResolver.MAP_PRODUCT_INTERNAL_PREFIX.length();

			StringBuilder sb = new StringBuilder(internalPath.substring(0,
					beginPos));
			sb.append(ProductLinkResolver.MAP_PRODUCT_EXTERNAL_PREFIX);
			sb.append(internalPath.substring(endPos, endPos + 3));
			sb.append(internalPath.substring(endPos + 4, endPos + 7));
			sb.append(internalPath.substring(endPos + 8, endPos + 11));
			sb.append('/').append(internalPath.substring(endPos + 12));
			externalPath = sb.toString();

			log.debug("Map products: externalPath = " + externalPath);
		}
		return externalPath;
	}
}