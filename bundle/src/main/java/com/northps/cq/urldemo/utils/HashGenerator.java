package com.northps.cq.urldemo.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HashGenerator {
	private static Logger log = LoggerFactory.getLogger(HashGenerator.class);
	private static MessageDigest md;
	
	static {
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			log.error("No such algorithm: MD5");
		}
	}
	
	public static String genHash(String input) {
		md.update(input.getBytes(), 0, input.length());
		return new BigInteger(1, md.digest()).toString(16);
	}
	
	public static String hashLast(String input, int len) {
		String hash = genHash(input);
		return hash.substring(hash.length() - len);
	}
}
