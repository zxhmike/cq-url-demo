package com.northps.cq.urldemo.shared;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationAction;
import com.northps.cq.urldemo.utils.CacheInvalidator;

@Component(immediate = true)
@Service
@Property(name = EventConstants.EVENT_TOPIC, value = ReplicationAction.EVENT_TOPIC)
public class ProductReplicationListener implements EventHandler{
	private static final String DISPATCHER_FLUSHES = "flush";
	
	@Reference
	CacheInvalidator invalidator;
	
    private static final Logger log = LoggerFactory.getLogger(ProductReplicationListener.class);
    
	public void handleEvent(Event event){
		ReplicationAction action = ReplicationAction.fromEvent(event);
		if (action == null) {
			log.error("No replication action found.");
			return;
		}
		
		String internalPath = action.getPath();
		log.debug("internalPath = " + internalPath);
		String externalPath = ProductLinkResolver.map(internalPath);
		if (externalPath == null) {
			log.debug("Nothing special happens. Quit.");
			return;
		}
		log.debug("externalPath = " + externalPath);
		
		String[] flushes = ProductReplicationListener.DISPATCHER_FLUSHES.split(",");
		invalidator.invalidateWithVanities(flushes, externalPath);
	}
}