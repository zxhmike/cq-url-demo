package com.northps.cq.urldemo.shared;

import java.io.IOException;

import org.apache.sling.rewriter.ProcessingComponentConfiguration;
import org.apache.sling.rewriter.ProcessingContext;
import org.apache.sling.rewriter.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.day.cq.rewriter.pipeline.AbstractContentHandler;
import com.day.cq.rewriter.pipeline.AttributesImpl;

public class ProductTransformer extends AbstractContentHandler implements
		Transformer {
	private static Logger log = LoggerFactory
			.getLogger(ProductTransformer.class);

	public void dispose() {
		// Do nothing
	}

	public void init(ProcessingContext context,
			ProcessingComponentConfiguration confix) throws IOException {
		// Do nothing
	}

	@Override
	public void startElement(final String uri, final String name,
			final String raw, final Attributes attrs) throws SAXException {
		final AttributesImpl attributes = new AttributesImpl(attrs);
		final String href = attributes.getValue("href");

		if (href != null) {
			final String externalPath = ProductLinkResolver.map(href);
			log.debug("Filter products: href = " + href);
			if (externalPath != null) {
				final Integer hrefIdx = attributes.getIndex("href");
				attributes.setValue(hrefIdx, externalPath);
				log.debug("Filter products: externalPath = " + externalPath);
			} else {
				log.debug("Filter products: nothing happens. keep " + href);
			}
		}

		super.startElement(uri, name, raw, attributes);
	}
}