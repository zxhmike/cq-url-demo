package com.northps.cq.urldemo.generatedproduct;
import java.util.Iterator;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;

public class ProductPageProperty implements Resource {
	private final Resource productResource;
	private final String parentResourceType;
	private final String key;
	private final Object value;
	private ResourceMetadata metadata;

	public ProductPageProperty(Resource productResource, String parentResourceType, String key, Object value) {
		this.productResource = productResource;
		this.parentResourceType = parentResourceType;
		this.key = key;
		this.value = value;
        metadata = new ResourceMetadata();
        metadata.setResolutionPath(productResource.getPath() + "/" + "jcr:content/" + key);
	}

	public <AdapterType> AdapterType adaptTo(Class<AdapterType> type) {
        return null;
	}
	
	public String toString() {
		return value.toString();
	}

	public String getPath() {
		return metadata.getResolutionPath();
	}

	public String getName() {
		// I pretend to be a "JCR" content although I am not.
		// This is because CQ code was written without in mind
		// the fact that a Page might not be a JCR node.
		return key; 
	}

	public Resource getParent() {
		return productResource.getChild("jcr:content");
	}

	public Iterator<Resource> listChildren() {
		return null;
	}

	public Iterable<Resource> getChildren() {
		return null;
	}

	public Resource getChild(String relPath) {
		return null;
	}

	public String getResourceType() {
		return parentResourceType + "/" + key;
	}

	public String getResourceSuperType() {
		return null;
	}

	public boolean isResourceType(String resourceType) {
		return resourceType.equals(getResourceType());
	}

	public ResourceMetadata getResourceMetadata() {
		return metadata;
	}

	public ResourceResolver getResourceResolver() {
		return productResource.getResourceResolver();
	}
}