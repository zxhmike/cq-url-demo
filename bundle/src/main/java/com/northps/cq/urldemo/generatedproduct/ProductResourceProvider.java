package com.northps.cq.urldemo.generatedproduct;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceProvider;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.SyntheticResource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
@Service
@Properties({
    @Property(name=ResourceProvider.ROOTS, value=ProductResourceProvider.ROOT),
})
public class ProductResourceProvider implements ResourceProvider {
	public static final String ROOT = "/content/url-demo/generated-products";
	private static final Logger log = LoggerFactory.getLogger(ProductResourceProvider.class);
	
	private static Map<String, ValueMap> products;
	private static void addProduct(String key, String name, String description, String price) {
		ValueMap valueMap = new ValueMapDecorator(new HashMap<String, Object>());
		valueMap.put("name", name);
		valueMap.put("description", description);
		valueMap.put("price", price);
		
		valueMap.put("jcr:title", "name");
		valueMap.put("cq:lastModified", new GregorianCalendar(2014, 1, 4));
		valueMap.put("cq:lastModifiedBy", "admin");
		valueMap.put("jcr:created", new GregorianCalendar(2014, 1, 4));
		valueMap.put("jcr:createdBy", "admin");
		valueMap.put("sling:resourceType", ProductResource.RESOURCE_TYPE);
		valueMap.put("jcr:primaryType", "cq:PageContent");
		
		products.put(key, valueMap);
	}
	
	static {
		products = new HashMap<String, ValueMap>();
		addProduct("circle", "Generated Circle", "This is a generated circle.", "27.68");
		addProduct("square", "Generated Square", "This is a generated square.", "23.68");
		addProduct("triangle", "Generated Triangle", "This is a generated triangle.", "21.68");
	}

	public Resource getResource(ResourceResolver rr, String path) {
		log.debug("path = " + path);
		if (path.equals(ProductResourceProvider.ROOT)) {
			return new SyntheticResource(rr, path, "nt:folder");
		} else {
			String key = path.substring(ProductResourceProvider.ROOT.length() + 1);
			ValueMap valueMap = products.get(key);
			return valueMap == null ? null : new ProductResource(rr, path, valueMap);
		}
	}

	@Deprecated
	public Resource getResource(ResourceResolver rr, HttpServletRequest request,
			String path) {
		return getResource(rr, path);
	}

	public Iterator<Resource> listChildren(Resource resource) {
		// TODO: might be better to return "jcr:content"
		return null;
	}
}