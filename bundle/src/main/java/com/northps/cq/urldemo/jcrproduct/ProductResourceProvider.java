package com.northps.cq.urldemo.jcrproduct;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceProvider;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.SyntheticResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.northps.cq.urldemo.shared.ProductLinkResolver;

@Component
@Service
@Properties({
    @Property(name=ResourceProvider.ROOTS, value=ProductResourceProvider.ROOT),
})
public class ProductResourceProvider implements ResourceProvider {
	public static final String ROOT = "/content/url-demo/jcr-products";
	private static final Logger log = LoggerFactory.getLogger(ProductResourceProvider.class);
	
	public Resource getResource(ResourceResolver rr, String path) {
		log.debug("path = " + path);
		if (path.equals(ProductResourceProvider.ROOT)) {
			return new SyntheticResource(rr, path, "nt:folder");
		} else {
			String internalPath = ProductLinkResolver.resolve(path);
			Resource resource = rr.resolve(internalPath);

			// Have to wrap the actual JcrNodeResource with a new path.
			// Otherwise the CQ link transformer will rewrite the link to its actual JCR path.
			return new ProductResource(resource, path);
		}
	}

	@Deprecated
	public Resource getResource(ResourceResolver rr, HttpServletRequest request,
			String path) {
		return getResource(rr, path);
	}

	public Iterator<Resource> listChildren(Resource resource) {
		return resource.listChildren();
	}
}