package com.northps.cq.urldemo.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.AgentManager;
import com.day.cq.replication.Replicator;

@Component
@Service(value=CacheInvalidator.class)
@Properties({
	@Property(name = "service.pid", value = "com.northps.cq.urldemo.utils.cacheinvalidator", propertyPrivate = false),
	@Property(name = "service.description", value = "URL Demo Cache Invalidator", propertyPrivate = false),
	@Property(name = "service.vendor", value = "NorthPoint", propertyPrivate = false) })
public class CacheInvalidatorImpl implements CacheInvalidator {
	static private Logger log = LoggerFactory.getLogger(CacheInvalidatorImpl.class);
	
	@Reference
	AgentManager agentManager;
	
	@Reference
	Replicator replicator;
	
	@Reference
	SlingRepository repository;
	
	private Session session = null;
	
	protected void activate(ComponentContext componentContext) {  
		try {
			session = repository.loginAdministrative(repository.getDefaultWorkspace());
		} catch (RepositoryException e) {
			log.error("Cannot login to JCR session.");
		}
	}
	
	protected void deactivate(ComponentContext componentContext) {  
		session.logout();
	}
	
	private class Agent {
		private static final String BASIC_PATH = "/etc/replication/agents.author";
		private String path;

		public Agent(String conf) {
			int left = conf.indexOf('(');
			int right = conf.indexOf(')');
			int mid = conf.indexOf(':');
			if (left != -1 && right != -1 && mid != -1 && left < mid && mid < right) {
				this.path = Agent.BASIC_PATH + "/" + conf.substring(0, left);
			} else {
				this.path = Agent.BASIC_PATH + "/" + conf;
			}
		}
		
		public String getPath() {
			return path;
		}
	}
	
	public void invalidate(String[] flushes, String... toInvalidatePaths) {
		log.info("Invalidating cache for path: " + Arrays.toString(toInvalidatePaths));			
		
		for (String flush : flushes) {
			for (String path : toInvalidatePaths) {
				try {
					Agent agent = new Agent(flush);
					Node agentNode = session.getNode(agent.getPath() + "/jcr:content");
					String uri = agentNode.getProperty("transportUri").getString();
					HttpMethod method = new GetMethod(uri);
					
					method.setRequestHeader("CQ-Action", "Activate");
					method.setRequestHeader("CQ-Handle", path);
					method.setRequestHeader("CQ-Path", path);
					
					if (agentNode.hasProperty("protocolHTTPHeaders")) {
						javax.jcr.Property property = agentNode.getProperty("protocolHTTPHeaders");
						Value[] headers;
						if (property.isMultiple()) {
							headers = property.getValues();
						} else {
							headers = new Value[]{property.getValue()};
						}
							
						for (Value header : headers) {
							String[] conf = header.getString().split(":");
							if (conf[1].contains("{action}")) {
								conf[1] = "Activate";
							}
							if (conf[1].contains("{path}")) {
								conf[1] = path;
							}
							method.setRequestHeader(conf[0], conf[1]);
							
							if (conf[0].equalsIgnoreCase("HOST")) {
								method.getParams().setVirtualHost(conf[1].replaceAll(" ", ""));
							}
						}
					}
					
					HttpClient client = new HttpClient();
					client.executeMethod(method);
				} catch (PathNotFoundException e) {
					log.error("Error invalidating the cache: replicator not found");
				} catch (RepositoryException e) {
					log.error("Error invalidating the cache: RepositoryException");
				} catch (HttpException e) {
					log.error("Error invalidating the cache: HttpException");
				} catch (IOException e) {
					log.error("Error invalidating the cache: IOException");
				}
			}
		}
	}

	public void invalidateWithVanities(String[] flushes,
			String... toInvalidatePaths) {
		List<String> paths = new ArrayList<String>();
		
		for (String path : toInvalidatePaths) {
			paths.add(path);
			Node node = null;
			try {
				node = session.getNode(path + "/jcr:content");
			} catch (PathNotFoundException e1) {
				node = null;
			} catch (RepositoryException e1) {
				node = null;
			}
				
			try {
				boolean hasVanities = node != null && node.hasProperty("sling:vanityPath");
				if (hasVanities) {
					javax.jcr.Property vanitiesPropterty = node.getProperty("sling:vanityPath");
					if (vanitiesPropterty.isMultiple()) {
						Value[] vanities = node.getProperty("sling:vanityPath").getValues();
						for (Value vanity: vanities) {
							String vanityString = vanity.getString();
							paths.add((vanityString.startsWith("/") ? "" : "/") + vanity.getString());
						}
					} else {
						String vanityString = node.getProperty("sling:vanityPath").getString();
						paths.add((vanityString.startsWith("/") ? "" : "/") + vanityString);
					}
				}
			} catch (PathNotFoundException e) {
				log.error("Path not found.");
			} catch (RepositoryException e) {
				log.error("RepositoryException");
			}
		}
		
		invalidate(flushes, paths.toArray(new String[paths.size()]));
	}
}