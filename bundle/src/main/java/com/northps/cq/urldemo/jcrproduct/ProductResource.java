package com.northps.cq.urldemo.jcrproduct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceWrapper;

public class ProductResource extends ResourceWrapper {
	private String actualPath;

	public ProductResource(Resource resource) {
		super(resource);
	}

	public ProductResource(Resource resource, String actualPath) {
		super(resource);
		this.actualPath = actualPath;
	}
	
	public String getPath() {
		return actualPath; 
	}
}
