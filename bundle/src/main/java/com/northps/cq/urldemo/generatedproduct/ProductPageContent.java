package com.northps.cq.urldemo.generatedproduct;
import java.util.Iterator;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

public class ProductPageContent implements Resource {
	private final Resource productResource;
	private final ValueMap valueMap;
	private ResourceMetadata metadata;

	public ProductPageContent(Resource productResource, ValueMap valueMap) {
		this.productResource = productResource;
		this.valueMap = valueMap;
        metadata = new ResourceMetadata();
        metadata.setResolutionPath(productResource.getPath() + "/" + "jcr:content");
	}

	public <AdapterType> AdapterType adaptTo(Class<AdapterType> type) {
		return null;
	}

	public String getPath() {
		return metadata.getResolutionPath();
	}

	public String getName() {
		// I pretend to be a "JCR" content although I am not.
		// This is because CQ code was written without in mind
		// the fact that a Page might not be a JCR node.
		return "jcr:content";
	}

	public Resource getParent() {
		return productResource;
	}

	public Iterable<Resource> getChildren() {
		return null;
		// Looks like jcr:content node returns null
		// Now I am returning null too.
//		return new Iterable<Resource>() {
//
//			public Iterator<Resource> iterator() {
//				return new Iterator<Resource>() {
//					private ValueMap map;
//					private Iterator<Entry<String, Object>> iterator;
//					{
//						this.map = valueMap;
//						iterator = map.entrySet().iterator();
//					}
//		
//					public boolean hasNext() {
//						return iterator.hasNext();
//					}
//		
//					public Resource next() {
//						String key = iterator.next().getKey();
//						return new ProductPageProperty(productResource, getResourceType(), key, map.get(key));
//					}
//		
//					public void remove() {
//						// Not supported. Do nothing.
//					}
//				};
//			}
//		};
	}

	public Iterator<Resource> listChildren() {
		// Save as above
		return null;
		//return getChildren().iterator();
	}

	public Resource getChild(String relPath) {
		Object value = valueMap.get(relPath);
		return value == null ? null : new ProductPageProperty(productResource, getResourceType(), relPath, value);
	}

	public String getResourceType() {
		return (String)valueMap.get("sling:resourceType");
	}

	public String getResourceSuperType() {
		return null;
	}

	public boolean isResourceType(String resourceType) {
		return resourceType.equals(getResourceType());
	}

	public ResourceMetadata getResourceMetadata() {
		return metadata;
	}

	public ResourceResolver getResourceResolver() {
		return productResource.getResourceResolver();
	}
}