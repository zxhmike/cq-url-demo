package com.northps.cq.urldemo.filterproduct;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.northps.cq.urldemo.shared.ProductLinkResolver;

@Component
@Service
@Properties({ @Property(name = "sling.filter.scope", value = "REQUEST") })
public class ProductFilter implements javax.servlet.Filter {

	private static final Logger log = LoggerFactory
			.getLogger(ProductFilter.class);

	public void destroy() {
		// TODO Auto-generated method stub

	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String externalPath = httpRequest.getRequestURI();
		log.debug("external path = " + externalPath);

		final String internalPath = ProductLinkResolver.resolve(externalPath);
		if (internalPath != null) {
			log.debug("internal path = " + internalPath);
			httpRequest.getRequestDispatcher(internalPath)
					.forward(request, response);
		} else {
			log.debug("Nothing special. Go to the next filter.");
			filterChain.doFilter(request, response);
		}
	}

	public void init(FilterConfig arg0) throws ServletException {
		// Nothing to do
	}
}